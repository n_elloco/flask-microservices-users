from sqlalchemy.exc import IntegrityError

from project.tests.base import BaseTestCase
from project.tests.utils import add_user


class TestUserModel(BaseTestCase):

    def test_add_user(self):
        user = add_user('test@test.com', 'test@test.com', 'test')

        self.assertTrue(user.id)
        self.assertEqual(user.username, 'test@test.com')
        self.assertEqual(user.email, 'test@test.com')
        self.assertTrue(user.password)
        self.assertTrue(user.active)
        self.assertTrue(user.created_at)

    def test_add_user_duplicate_username(self):
        add_user('test@test.com', 'test@test.com', 'test')
        with self.assertRaises(IntegrityError):
            add_user('test@test.com', 'test2@test.com', 'test')

    def test_add_user_duplicate_email(self):
        add_user('test@test.com', 'test@test.com', 'test')
        with self.assertRaises(IntegrityError):
            add_user('test2@test.com', 'test@test.com', 'test')

    def test_passwords_are_random(self):
        user_one = add_user('test@test.com', 'test@test.com', 'test')
        user_two = add_user('test2@test.com', 'test2@test.com', 'test')
        self.assertNotEqual(user_one.password, user_two.password)
